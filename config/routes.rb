Rails.application.routes.draw do

  resources :transactions
  # devise_for :admins
  
  # match ':controller(/:action(/:id))', :via => [:get, :post]
  # get 'static_pages/home'
  # get 'static_pages/about'
  # get 'static_pages/contact'
  # get 'static_pages/news'
  
  root 'static_pages#home'
  get  '/news',    to: 'static_pages#news'
  get  '/about',   to: 'static_pages#about'
  get  '/contact', to: 'static_pages#contact'

  get  '/userlist', to: 'authentication#index'

  # match '/authentication/update/:id', to: 'authentication#update', via: :post


  # resources :account
  get   '/account', to: 'account#index'
  get   '/account/:id/show', to: 'account#show'
  get   '/account/all', to: 'account#all_index'
  
  get   '/account/new', to: 'account#new'
  post   '/account', to: 'account#create'
  get   '/account/:id/edit', to: 'account#edit'
  post   '/account/:id', to: 'account#update'

  get    '/transactions/fund-transfer/new', to: 'transactions#fund_transfer'
  post    '/transactions/fund-transfer', to: 'transactions#transfer'
  get     'transactions/paybill/new',   to: 'transactions#paybill'
  post    'transactions/paybill',       to: 'transactions#pay'

  get     '/transactions/cash-withdraw/new', to: 'transactions#cash_withdraw'
  post    '/transactions/cash-withdraw', to: 'transactions#withdraw'
  get     '/transactions/cash-deposit/new', to: 'transactions#cash_deposit'
  post    '/transactions/cash-deposit', to: 'transactions#deposit'

  get     '/transactions/income/new', to: 'transactions#income'
  post    '/transactions/income', to: 'transactions#create_income'
  get     '/transactions/expenses/new', to: 'transactions#expenses'
  post    '/transactions/expenses', to: 'transactions#create_expenses'

  get   '/transactions/:id/auth', to: 'transactions#auth', as: 'auth'
  post   '/transactions/:id', to: 'transactions#authorise'
  
  get '/unau_transactions', to: 'transactions#unau_list'  

  get '/users', to: 'users/session#index'
  get   '/unau_show/:id', to: 'transactions#unau_show'
  
  get 'audit/detail', to: 'audit#detail'
  get 'audit/index'
  get 'interest', to: 'interest#index'
  get 'interest/show'
  

  # devise_for :admins, :path => '', :path_names => { :sign_in => "login", :sign_out => "logout", :sign_up => "register" }
  devise_for :admins, :path => '', :path_names => { :sign_in => "login", :sign_out => "logout", :sign_up => "register" }


# devise_for :users, path: "", 
#   controllers: 
#   { 
#     sessions: "sessions", 
#     registrations: "registrations" 
#   }, 
#   path_names: 
#   { 
#     sign_in: 'login', 
#     password: 'forgot', 
#     confirmation: 'confirm', 
#     unlock: 'unblock', 
#     sign_up: 'register', 
#     sign_out: 'signout'
#   }

  # get  'new', to: 'admins/registrations#new'
  #   edit_admin_registration GET    /admins/edit(.:format)          devise/registrations#edit
  
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
