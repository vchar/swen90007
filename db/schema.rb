# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161008080402) do

  create_table "account_entries", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "account_id"
    t.index ["account_id"], name: "index_account_entries_on_account_id"
  end

  create_table "accounts", force: :cascade do |t|
    t.integer  "account_number"
    t.string   "account_name"
    t.float    "interest_rate"
    t.float    "last_credit",     default: 0.0
    t.float    "last_debit",      default: 0.0
    t.string   "status"
    t.string   "created_by"
    t.string   "updated_by"
    t.integer  "account_type"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "user_id"
    t.float    "working_balance", default: 0.0
    t.float    "actual_balance",  default: 0.0
    t.float    "tax_rate",        default: 0.1
    t.index ["user_id"], name: "index_accounts_on_user_id"
  end

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "username"
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
    t.index ["username"], name: "index_admins_on_username", unique: true
  end

  create_table "interests", force: :cascade do |t|
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "account_id"
    t.float    "debit_interest"
    t.float    "credit_interest"
    t.float    "interest_rate"
    t.date     "date_start"
    t.date     "date_end"
    t.float    "tax_rate"
    t.float    "tax_amount"
    t.float    "account_balance"
    t.index ["account_id"], name: "index_interests_on_account_id"
  end

  create_table "taxes", force: :cascade do |t|
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "interest_id"
    t.string   "reference"
    t.integer  "account_number"
    t.float    "tax_amount"
    t.string   "account_name"
    t.integer  "account_type"
    t.date     "date_start"
    t.date     "date_end"
    t.float    "interest_amount"
    t.float    "interest_rate"
    t.float    "tax_rate"
    t.float    "account_balance"
    t.boolean  "is_audited",      default: false
    t.date     "last_audited"
    t.integer  "audited_by"
    t.string   "audited_by_name"
    t.index ["interest_id"], name: "index_taxes_on_interest_id"
  end

  create_table "transactions", force: :cascade do |t|
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "transaction_type"
    t.integer  "debit_account"
    t.integer  "credit_account"
    t.float    "debit_amount"
    t.float    "credit_amount"
    t.date     "effective_date"
    t.string   "created_by"
    t.string   "authorised_by"
    t.date     "authorised_date"
    t.string   "status",           default: "Unau", null: false
    t.string   "external_ref"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: ""
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "username"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "address"
    t.string   "contact_number"
    t.string   "status"
    t.string   "position"
    t.integer  "department"
    t.string   "occupation"
    t.string   "company"
    t.string   "created_by"
    t.string   "updated_by"
    t.integer  "role"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

end
