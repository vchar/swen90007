class AddInterestToTax < ActiveRecord::Migration[5.0]
  def change
  	add_reference :taxes, :interest, index: true
  end
end
