class AddFieldToInterest < ActiveRecord::Migration[5.0]
  def change
  	add_column :interests, :date_start, :date
  	add_column :interests, :date_end, :date
  	add_column :interests, :tax_rate, :float
  	add_column :interests, :tax_amount, :float
  end
end
