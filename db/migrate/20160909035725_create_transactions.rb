class CreateTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :transactions do |t|
      t.string :index
      t.string :show
      t.string :create
      t.string :update

      t.timestamps
    end
  end
end
