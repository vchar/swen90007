class ChangeTypeToRole < ActiveRecord::Migration[5.0]
  def change
  	remove_column :users, :type
  	add_column :users, :role, :integer
  end
end
