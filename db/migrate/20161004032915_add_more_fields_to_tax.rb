class AddMoreFieldsToTax < ActiveRecord::Migration[5.0]
  def change
  	add_column :taxes, :account_name, :string
  	add_column :taxes, :account_type, :integer
  	add_column :taxes, :date_start, :date
  	add_column :taxes, :date_end, :date
  	add_column :taxes, :interest_amount, :float
  	add_column :taxes, :interest_rate, :float
  	add_column :taxes, :tax_rate, :float
  	add_column :taxes, :account_balance, :float
  end
end
