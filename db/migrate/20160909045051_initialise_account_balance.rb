class InitialiseAccountBalance < ActiveRecord::Migration[5.0]
  def change
  	remove_column :accounts, :working_balance
  	remove_column :accounts, :actual_balance
  	add_column :accounts, :working_balance, :double, default: 0.0
  	add_column :accounts, :actual_balance, :double, default: 0.0
  end
end
