class AddProfileFieldsToUsers < ActiveRecord::Migration[5.0]
  def change
  	add_column :users, :first_name, :string
  	add_column :users, :last_name, :string
  	add_column :users, :type, :integer
  	add_column :users, :address, :string
  	add_column :users, :contact_number, :string
  	add_column :users, :status, :string
  	add_column :users, :position, :string
  	add_column :users, :department, :integer
  	add_column :users, :occupation, :string                    
  	add_column :users, :company, :string
  	add_column :users, :created_by, :string
  	add_column :users, :updated_by, :string
  end
end
