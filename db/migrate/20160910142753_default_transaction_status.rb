class DefaultTransactionStatus < ActiveRecord::Migration[5.0]
  def change
  	remove_column :transactions, :status
  	add_column :transactions, :status, :string, null: false, default: "Unau"
  end
end
