class AddFieldsToInterest < ActiveRecord::Migration[5.0]
  def change

  	add_column :interests, :debit_interest, :float
  	add_column :interests, :credit_interest, :float
  	add_column :interests, :interest_rate, :float
  end
end
