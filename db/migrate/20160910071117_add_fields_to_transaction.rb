class AddFieldsToTransaction < ActiveRecord::Migration[5.0]
  def change
  	remove_column :transactions, :index		
  	remove_column :transactions, :show
  	remove_column :transactions, :create
  	remove_column :transactions, :update
  	add_column :transactions, :transaction_type, :integer
  	add_column :transactions, :debit_account, :integer
  	add_column :transactions, :credit_account, :integer
  	add_column :transactions, :debit_amount, :double
  	add_column :transactions, :credit_amount, :double
  	add_column :transactions, :effective_date, :date
  	add_column :transactions, :status, :string
  	add_column :transactions, :created_by, :string
  	add_column :transactions, :authorised_by, :string
  	add_column :transactions, :authorised_date, :date 

  	# add_column :transactions, :debit_account, :integer, default: 0.0
  end
end
