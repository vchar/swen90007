class AddAccountToInterest < ActiveRecord::Migration[5.0]
  def change
  	add_reference :interests, :account, index: true
  end
end
