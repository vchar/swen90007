class ChangeAuditedToBoolean < ActiveRecord::Migration[5.0]
  def change
  	change_column :taxes, :is_audited, :boolean, :default => false
  end
end
