class AddFieldsToTax < ActiveRecord::Migration[5.0]
  def change
  	add_column :taxes, :reference, :string
  	add_column :taxes, :account_number, :integer
  	add_column :taxes, :tax_amount, :float
  end
end
