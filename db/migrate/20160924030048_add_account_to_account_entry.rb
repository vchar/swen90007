class AddAccountToAccountEntry < ActiveRecord::Migration[5.0]
  def change
  	add_reference :account_entries, :account, index: true
  end
end
