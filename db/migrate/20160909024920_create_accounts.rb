class CreateAccounts < ActiveRecord::Migration[5.0]
  def change
    create_table :accounts do |t|
      t.integer :account_number
      t.string :account_name
      t.float :interest_rate
      t.float :working_balance
      t.float :actual_balance
      t.float :last_credit,   default: 0.0
      t.float :last_debit,    default: 0.0
      t.string :status
      t.string :created_by
      t.string :updated_by
      t.integer :account_type

      t.timestamps
    end
  end
end
