class AddAuditorToTax < ActiveRecord::Migration[5.0]
  def change
  	add_column :taxes, :is_audited, :boolean, :default => false
  	add_column :taxes, :last_audited, :date
  	add_column :taxes, :audited_by, :integer
  end
end
