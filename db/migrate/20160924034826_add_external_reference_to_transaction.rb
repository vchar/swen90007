class AddExternalReferenceToTransaction < ActiveRecord::Migration[5.0]
  def change
  	add_column :transactions, :external_ref, :string
  end
end
