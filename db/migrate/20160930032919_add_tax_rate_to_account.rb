class AddTaxRateToAccount < ActiveRecord::Migration[5.0]
  def change
  	add_column :accounts, :tax_rate, :float, :default => 0.1
  end
end
