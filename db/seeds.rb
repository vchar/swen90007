# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Creates seeded persons for staff and customers and logging information to console.
def create_account
  puts('### CREATING ACCOUNTS ###')
  ACCOUNTS.each do |account_number, account_name, account_type, user_id|
    puts('  ' + account_number.to_s.ljust(3) + account_name.ljust(16) + account_type.to_s.ljust(3))
    User.find(1).accounts.create(account_number: account_number, account_name: account_name, account_type: account_type)
  end
  puts()
end

# Creates seeded persons for staff and customers and logging information to console.
def create_persons
  puts('### CREATING PERSONS ###')
  PERSONS.each do |username, password, email, role, firstname, lastname, address, phone|
    puts('  ' + username.ljust(15) + password.ljust(7) + email.ljust(42) + role.to_s.ljust(2) + 
        firstname.ljust(11) + lastname.ljust(14) + address.ljust(57) + phone.ljust(12))
    User.create(username: username, password: password, email: email, role: role, 
        first_name: firstname, last_name: lastname, address: address, contact_number: phone)
  end
  puts()
end

# Creates account for all customer
# for random seeding customer account purpose...
def create_customer_account

	persons = User.where(role: 'customer')

	persons.each do |person|
		balance = rand(-1000..5000)
		person.accounts.create(account_name: "#{person.username} DEFAULT", account_number: rand(11111..99999),  
								account_type: 3, actual_balance: balance, working_balance: balance, interest_rate: 0.02)
	end

end

# account_number, account_name, account_type, user, user_id
ACCOUNTS = [
	[1, 'Internal Debit', 0],
	[2, 'Internal Credit', 0],
	[3, 'Profit', 1],
	[4, 'Loss', 1],
	[5, 'External Debit', 2],
	[6, 'External Credit', 2],
	[7, 'Tax Debit', 0],
	[8, 'Tax Credit', 0],
	[9, 'Interest Payable', 1], #debit
	[10, 'Interest Receivable', 1] #credit
]

# Staff and customers information
# username, password, email, admin?, role, firstname, lastname, contact_number, address 
PERSONS = [
	['omgbank','omgbank','admin@omgbank.com.au',1,'Omg','Bank','Melbourne, Victoria 3000','9999-9999'],
	['rdidio','123456','rebbecca.didio@didio.com.au',1,'Rebbecca','Didio','171 E 24th St, Leith, TA, 7315','03-8174-9123'],
	['shallo','123456','stevie.hallo@hotmail.com',2,'Stevie','Hallo','22222 Acoma St, Proston, QL, 4613','07-9997-3366'],
	['mstayer','123456','mariko_stayer@hotmail.com',2,'Mariko','Stayer','534 Schoenborn St #51, Hamel, WA, 6215','08-5558-9019'],
	['gwoodka','123456','gerardo_woodka@hotmail.com',2,'Gerardo','Woodka','69206 Jackson Ave, Talmalmo, NS, 2640','02-6044-4682'],
	['mbena','123456','mayra.bena@gmail.com',2,'Mayra','Bena','808 Glen Cove Ave, Lane Cove, NS, 1595','02-1455-6085'],
	['iscotland','123456','idella@hotmail.com',1,'Idella','Scotland','373 Lafayette St, Cartmeticup, WA, 6316','08-7868-1355'],
	['sklar','123456','sklar@hotmail.com',2,'Sherill','Klar','87 Sylvan Ave, Nyamup, WA, 6258','08-6522-8931'],
	['edesjardiws','123456','ena_desjardiws@desjardiws.com.au',1,'Ena','Desjardiws','60562 Ky Rt 321, Bendick Murrell, NS, 2803','02-5226-9402'],
	['vsiena','123456','vince_siena@yahoo.com',2,'Vince','Siena','70 S 18th Pl, Purrawunda, QL, 4356','07-3184-9989'],
	['tjarding','123456','tjarding@hotmail.com',1,'Theron','Jarding','8839 Ventura Blvd, Blanchetown, SA, 5357','08-6890-4661'],
	['achudej','123456','amira.chudej@chudej.net.au',2,'Amira','Chudej','3684 N Wacker Dr, Rockside, QL, 4343','07-8135-3271'],
	['mtarbor','123456','marica.tarbor@hotmail.com',1,'Marica','Tarbor','68828 S 32nd St #6, Rosegarland, TA, 7140','03-1174-6817'],
	['salbrough','123456','shawna.albrough@albrough.com.au',2,'Shawna','Albrough','43157 Cypress St, Ringwood, QL, 4343','07-7977-6039'],
	['pmaker','123456','paulina_maker@maker.net.au',2,'Paulina','Maker','6 S Hanover Ave, Maylands, WA, 6931','08-8344-8929'],
	['rjebb','123456','rose@jebb.net.au',2,'Rose','Jebb','27916 Tarrytown Rd, Wooloowin, QL, 4030','07-4941-9471'],
	['rtabar','123456','rtabar@hotmail.com',1,'Reita','Tabar','79620 Timber Dr, Arthurville, NS, 2820','02-3518-7078'],
	['mbewley','123456','mbewley@yahoo.com',2,'Maybelle','Bewley','387 Airway Cir #62, Mapleton, QL, 4560','07-9387-7293'],
	['officer1', '123456', 'officer1@gmail.com', 3, 'Tom', 'Hank','404 Address Not Found, GGWP 420', '404040404'],
	['officer2', '123456', 'officer2@gmail.com', 3, 'Brad', 'Pitt','404 Address Not Found, GGWP 420', '404040404'],
	['officer3', '123456', 'officer3@gmail.com', 3, 'Mike', 'Tyson','404 Address Not Found, GGWP 420', '404040404'],
]


create_persons
create_account
create_customer_account