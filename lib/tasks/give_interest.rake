namespace :give_interest do

	# Usage: $ rake give_interest:auto
	desc "Give interest to all customer account automatically"
	task :auto => :environment do

		puts "giving free $$$..."

		(Account.all).each do |account|

			# skip if the account has no interest rate
			# skip if it is not customer account
			if (account.interest_rate.nil? or ['internal', 'pl', 'external'].include? account.account_type)
				puts "skipped bank system account :: #{account.account_name}"
				next
			end

			# find the last interest paid to the account
			last_interest_entry = Interest.where(account_id: account.id).last
			# get the date start
			if (last_interest_entry.nil? or last_interest_entry.date_end.nil?)
				date_start = Date.today
			else
				date_start = last_interest_entry.date_end
			end
			# get date end, which is next month of date start
			date_end = date_start.next_month

			# credit the account if actual balance is non-negative
			if (account.actual_balance > 0)
				# credit the account
				credit_interest_transaction(account, date_start, date_end)
			else
				# debit the account
				debit_interest_transaction(account, date_start, date_end)
			end
		end
	end

	# Usage: $ rake give_interest:by_dates[12/3/2014,30/4/2015]
	desc "Give interest to all customer by starting date and ending date with format DD/MM/YYYY"
	task :by_dates, [:start, :end] => [:environment] do |t, args|
		
		# parse the dates
		date_start = Date.strptime(args.start, "%d/%m/%Y") 
		date_end = Date.strptime(args.end, "%d/%m/%Y")
		raise "=====\nERROR::ending date must be later than starting date!\n=====\n" if (date_start > date_end)

		puts "giving free $$$$ again..."
		(Account.all).each do |account|

			# skip if the account has no interest rate
			# skip if it is not customer account
			if (account.interest_rate.nil? or ['internal', 'pl', 'external'].include? account.account_type)
				puts "skipped bank system account :: #{account.account_name}"
				next
			end

			# Pay interest by month
			temp_date_start = date_start
			temp_date_end = date_start.next_month
			# escape if next month is large then the input date end
			while temp_date_end < date_end do
				# credit the account if actual balance is non-negative
				if (account.actual_balance > 0)
					# credit the account
					credit_interest_transaction(account, temp_date_start, temp_date_end)
				else
					# debit the account
					debit_interest_transaction(account, temp_date_start, temp_date_end)
				end
				# increment to next month
				temp_date_start = temp_date_end
				temp_date_end = temp_date_end.next_month
			end

			# Partial interest by day
			if (account.actual_balance > 0)
				# credit the account
				credit_interest_transaction(account, temp_date_start, date_end)
			else
				# debit the account
				debit_interest_transaction(account, temp_date_start, date_end)
			end

		end
	end

	# Usage: $ rake give_interest:by_month[12/3/2014,4]
	desc "Give interest to all customer by starting date and number of month that needs to calculate"
	task :by_month, [:start, :month] => [:environment] do |t, args|

		# parse date
		date_start = Date.strptime(args.start, "%d/%m/%Y")
		months = args.month.to_i

		puts "giving free $$$$$... again!!!"
		(Account.all).each do |account|

			# skip if the account has no interest rate
			# skip if it is not customer account
			if (account.interest_rate.nil? or ['internal', 'pl', 'external'].include? account.account_type)
				puts "skipped bank system account :: #{account.account_name}"
				next
			end

			# pay interest by each months
			while months > 0 do
				# credit the account if actual balance is non-negative
				if (account.actual_balance > 0)
					# credit the account
					credit_interest_transaction(account, date_start, date_start.next_month)
				else
					# debit the account
					debit_interest_transaction(account, date_start, date_start.next_month)
				end

				# decrement months number
				months -= 1
				# increment month of start date
				date_start = date_start.next_month
			end
		end
	end

end




# Create transaction for credit interest
def credit_interest_transaction account, date_start, date_end

	# days for interest
	days = (date_end - date_start).to_i

	# credit interest
	interest_amount = (account.actual_balance * account.interest_rate) * (days / 365.0)

	# tax amount
	tax_amount = interest_amount * account.tax_rate

	# credit interest transaction
	# credit the customer account
	# debit from Interest Payable
	interest_transaction = Transaction.new(debit_account: 9, debit_amount: interest_amount,
										credit_account: account.id, credit_amount: interest_amount,
										status: 'Auth', authorised_by: 'system', authorised_date: Date.today)
	# check update result
	success = update_account(interest_transaction)
	if success
		# save the transaction
		interest_transaction.save
		# add entry to interest
		account.interests.create(account_id: account.id, 
								interest_rate: account.interest_rate, credit_interest: interest_amount, debit_interest: 0, 
								date_end: date_end, date_start: date_start, 
								tax_rate: account.tax_rate, tax_amount: tax_amount, account_balance: account.actual_balance)
	end

	# debit tax amount from customer account
	# credit to Tax Credit
	tax_transaction = Transaction.new(debit_account: account.id, debit_amount: tax_amount,
									credit_account: 8, credit_amount: tax_amount,
									status: 'Auth', authorised_by: 'system', authorised_date: Date.today)
	# check update result
	success = update_account(tax_transaction)
	if success
		# save transaction
		tax_transaction.save
	end
end

# Create transaction for debit interest
def debit_interest_transaction account, date_start, date_end

	# days for interest
	days = (date_end - date_start).to_i

	# debit interest
	interest_amount = (account.actual_balance * account.interest_rate) * (days / 365.0)

	# debit interest transaction
	# debit from customer account
	# credit into Interest Receivable #10
	interest_transaction = Transaction.new(debit_account: account.id, debit_amount: interest_amount * (-1.0),
											credit_account: 10, credit_amount: interest_amount * (-1.0),
											status: 'Auth', authorised_by: 'system', authorised_date: Date.today)
	# check update result
	success = update_account(interest_transaction)
	if success
		# save transaction
		interest_transaction.save
		# add entry to interest
		account.interests.create(account_id: account.id,
								interest_rate: account.interest_rate, debit_interest: interest_amount, credit_interest: 0,
								date_end: date_end, date_start: date_start, tax_amount: 0, 
								tax_rate: account.tax_rate, account_balance: account.actual_balance)
	end

end

# Update accounts base on transaction, replicate from transaction_controller
# Removed some balance checking because system can wants to deduct negative balance
def update_account transaction
	dr_acc = transaction.debit_account
	dr_amt = transaction.debit_amount
	cr_acc = transaction.credit_account
	cr_amt = transaction.credit_amount

	dr_account = Account.find(dr_acc)
	cr_account = Account.find(cr_acc)

	dr_account.working_balance -= dr_amt
	dr_account.actual_balance -= dr_amt
	dr_account.last_debit = dr_amt

	cr_account.working_balance += cr_amt
	cr_account.actual_balance += cr_amt
	cr_account.last_debit = cr_amt

	puts "DEBIT ---- #{dr_amt}"
	puts "DEBIT ---- #{dr_account.account_name}::#{dr_account.id} --> working_balance: #{dr_account.working_balance}"
	puts "DEBIT ---- #{dr_account.account_name}::#{dr_account.id} --> actual_balance: #{dr_account.actual_balance}"
	puts "CREDIT ---- #{cr_account.account_name}::#{cr_account.id} --> working_balance: #{cr_account.working_balance}"
	puts "CREDIT ---- #{cr_account.account_name}::#{cr_account.id} --> actual_balance: #{cr_account.actual_balance}"

	if dr_account.save and cr_account.save
		return true
	end

  	return false
end
