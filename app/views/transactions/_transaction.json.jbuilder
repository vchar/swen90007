json.extract! transaction, :id, :index, :show, :create, :update, :created_at, :updated_at
json.url transaction_url(transaction, format: :json)