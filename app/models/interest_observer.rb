class InterestObserver

	def after_create(interest)
		# just logging
		if interest.credit_interest > 0
			puts "-----------------------------"
			puts "#{interest.account_id} gets credit #{interest.credit_interest} : from #{interest.date_start} to #{interest.date_end}"
			puts "-----------------------------"

			account = Account.find(interest.account_id)

			# add entry to Tax table
			interest.create_tax(interest_id: interest.id, account_number: interest.account_id, tax_amount: interest.tax_amount,
								account_balance: interest.account_balance, account_type: account.account_type, account_name: account.account_name,
								date_start: interest.date_start, date_end: interest.date_end, tax_rate: interest.tax_rate,
								interest_amount: interest.credit_interest, interest_rate: interest.interest_rate)
		else
			puts "-----------------------------"
			puts "#{interest.account_id} loss #{interest.debit_interest} : from #{interest.date_start} to #{interest.date_end}"
			puts "-----------------------------"
		end
	end

end