class Interest < ApplicationRecord
	include Wisper::Publisher

	after_create :publish_creation_successful

	belongs_to :account
	has_one :tax
	

	after_create do
		broadcast(:after_create, self)
	end


	private 

	def publish_creation_successful
		self.subscribe(InterestObserver.new)
	end

end
