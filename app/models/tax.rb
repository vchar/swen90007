class Tax < ApplicationRecord
	include Wisper::Publisher
	belongs_to :interest


	def audit user
		broadcast(:after_audit, self, user)
	end

end
