class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable

  devise :database_authenticatable, :registerable, :recoverable, :rememberable, 
         :trackable, :validatable, :authentication_keys => [:username]

  enum role: {staff: 1, customer: 2, officer: 3}

  has_many :accounts

  # def email_required?
  # 	false
  # end

  def email_changed?
  	false
  end
end
