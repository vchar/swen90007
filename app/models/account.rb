class Account < ApplicationRecord

	belongs_to :user
	has_many :account_entries
	has_many :interests
	
	# Reserve ()
	enum account_type: { internal: 0, pl: 1, external: 2, saving: 3, fix_deposit: 4, current: 5 }
	# 1  Internal Debit  0  
	# 2  Internal Credit 0  
	# 3  Profit          1  
	# 4  Loss            1
	# 5  External Debit  2
	# 6  External Credit 2

end
