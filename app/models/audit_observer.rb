class AuditObserver

	def after_audit(tax, user)
		puts "OfficerLog: #{user.first_name} #{user.last_name} inspecting #{tax.id}..."

		if !tax.is_audited
			puts "==========="
			puts "OfficerLog: #{user.first_name} #{user.last_name} audited #{tax.id}!"
			puts "==========="
			tax.last_audited = Date.today
			tax.is_audited = true
			tax.audited_by = user.id
			tax.audited_by_name = "#{user.first_name} #{user.last_name}"
			tax.save
		end
	end

end