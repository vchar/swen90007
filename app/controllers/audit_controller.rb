class AuditController < ApplicationController
	include Wisper::Publisher
	before_action :set_account 

	def index
		@taxes = Tax.all

		@taxes.each do |tax|
			# notify
			tax.subscribe(AuditObserver.new)
			tax.audit(current_user)
		end
	end

	def detail
		@interest = Interest.find(params[:id])
		#@tax = Tax.find(params[:id])
		@tax = @interest.tax
	end


	private

	def set_account
		@user = current_user
	end

end
