class AccountController < ApplicationController

	before_action :set_account


	def index
	end

	# show a account
	def show
		@account = Account.find(params[:id])

    if current_user.role != 'officer'
      @transactions = Transaction.where('debit_account = ? OR credit_account = ?', @account.id, @account.id)
    end
    
	end

	 # create a new account
	def new
  	@account = Account.new()
  	@account.account_name = current_user.first_name + " " + current_user.last_name
	end

	def create
		@account = @user.accounts.create(account_params)
	    #@account = Account.new(account_params)

	    if @account.save
	      redirect_to(:action => 'index')
	    else
	      render('new')
		end
	end

  # edit an account
  def edit
    @account = @user.accounts.find(params[:id])
  end

  # update an account
  def update
    # find an existing object using form parameters
    @account = Account.find(params[:id])
    # update the object
    if @account.update_attributes(account_params)
      redirect_to(:action => 'show', :id => @account.id)
    else
      # if update fails, rediplay the form so user can fix problems
      render('edit')
    end
  end

  private
	# Use callbacks to share common setup or constraints between actions.
    def set_account
  		@user = current_user
  		@accounts = current_user.accounts
      @all_accounts = Account.all
    end

  	def account_params
  		params.require(:account).permit(:id, :account_name, :working_balance, :actual_balance, :account_type)
  	end

end
