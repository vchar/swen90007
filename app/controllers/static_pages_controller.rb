class StaticPagesController < ApplicationController
  # Turn off user authentication for all actions in this controller
  skip_before_filter :authenticate_user!

  def home
  end

  def about
  end

  def contact
  end

  def news
  end
end
