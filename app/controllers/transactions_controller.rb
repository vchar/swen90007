class TransactionsController < ApplicationController
  before_action :set_transaction, only: [:show, :edit, :update, :destroy]


  # GET /transactions
  # GET /transactions.json
  def index
    @transactions = Transaction.where(status: 'Auth', created_by: current_user.username)
  end

  # GET /transactions
  # GET /transactions.json
  def unau_list
    @transactions = Transaction.where(status: 'Unau')
  end


  # GET /transactions/1
  # GET /transactions/1.json
  def show
  end

  def unau_show
  end

  # GET /transactions/new
  def new
    @transaction = Transaction.new
  end

  # POST /transactions
  # POST /transactions.json
  def create
    @transaction = Transaction.new(transaction_params)
    @transaction.created_by = current_user.username

    valid = validate_input(@transaction, transaction_params)
    save valid, 'show', 'new'
  end

  # for users to pay bill online
  def paybill
    @transaction = Transaction.new
  end

  def pay
    amount = transaction_params[:debit_amount]
    @transaction = Transaction.new(transaction_params.merge(:credit_account=> '6', :credit_amount=> amount,
      :created_by=>current_user.username, :status=>'Auth', :authorised_by=>current_user.username, :authorised_date=>Time.now))

    valid = validate_input(@transaction, transaction_params)
    # save valid, 'show', 'paybill'
    save valid, 'show', 'paybill', true
  end

  # for users to transfer money online
  def fund_transfer
    @transaction = Transaction.new
  end


  def transfer
    amount = transaction_params[:debit_amount]
    @transaction = Transaction.new(transaction_params.merge(:credit_amount=> amount,
      :created_by=>current_user.username, :status=>'Auth', :authorised_by=>current_user.username, :authorised_date=>Time.now))
    #@transaction.attributes(:transaction_type => 0)
    valid = validate_input(@transaction, transaction_params)
    # save valid, 'show', 'fund_transfer'
    save valid, 'show', 'fund_transfer', true
  end

  # perform cash withdrawal transaction
  def cash_withdraw
    @transaction = Transaction.new()
  end

  def withdraw
    amount = transaction_params[:debit_amount]
    @transaction = Transaction.new(transaction_params.merge(:credit_account=>2, :credit_amount=> amount, :created_by=>current_user.username))

    valid = validate_input(@transaction, transaction_params)

    save valid, 'show', 'cash_withdraw', false
  end

  def cash_deposit
    @transaction = Transaction.new()
  end

  def deposit
    amount = transaction_params[:credit_amount]
    @transaction = Transaction.new(transaction_params.merge(:debit_account=>1, :debit_amount=> amount, :created_by=>current_user.username))

    valid = validate_input(@transaction, transaction_params)

    save valid, 'show', 'cash_deposit', false
  end

  # GET /transactions/1/edit
  def edit
    @transactions = Transaction.find(params[:id])
  end

  # PATCH/PUT /transactions/1
  # PATCH/PUT /transactions/1.json
  def update
    valid = validate_input(@transaction, params)

    if not valid.nil?
      flash[:notice] = valid
      render :action=>'edit'
    elsif @transaction.update(transaction_params)
      flash[:notice] = 'Transaction was successfully updated.'
      render :action=>'show'
    else
      flash[:notice] = 'Error: unable to save the record!'
      render :action=>'edit'
    end
  end

  def income
    @transaction = Transaction.new
  end

  def create_income
    amount = transaction_params[:credit_amount]
    @transaction = Transaction.new(transaction_params.merge(
      :debit_account=> 1, :debit_amount=>amount, :credit_account=> 3, :created_by=>current_user.username))

    valid = validate_input(@transaction, transaction_params)
    save valid, 'show', 'income', false
  end

  def expenses
    @transaction = Transaction.new
  end

  def create_expenses
    amount = transaction_params[:debit_amount]
    @transaction = Transaction.new(transaction_params.merge(
      :credit_account=>2, :credit_amount=>amount, :debit_account=> 4, :created_by=>current_user.username))

    valid = validate_input(@transaction, transaction_params)
    save valid, 'show', 'expenses', false
  end

  def auth
    @transaction = Transaction.find(params[:id])
    @transaction.status = "Auth"
    @transaction.authorised_by = current_user.username
    @transaction.authorised_date = Time.now
  end 

  def authorise
    @transaction = Transaction.find(params[:id])
    puts auth_params[:authorised_by]
    puts auth_params[:authorised_date]

    if not authorise_transaction @transaction   # authorise the transaction
      flash[:notice] = 'Unable to authorise the transaction due to insufficient balance.'
      render :action=>'auth'
    else
      if @transaction.update({ :status => auth_params[:status], :authorised_by => auth_params[:authorised_by], :authorised_date => auth_params[:authorised_date] })
        flash[:notice] = 'Transaction was successfully authorised.'
        render :action=>'show'
      else
        flash[:notice] = 'Error: unable to authorise the record!' 
        render :action=>'edit'
      end
    end
  end

  # DELETE /transactions/1
  # DELETE /transactions/1.json
  def destroy
    if @transaction.status == "Unau"
      @transaction.destroy  
      respond_to do |format|
        format.html { redirect_to unau_transactions_url, notice: 'Transaction was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      flash[:notice] = 'Transaction was not destroyed.'
      render :action=>'index '
    end 
  end

  private
    # update relevant accounts for a transaction for staff
    def transact transaction
      dr_acc = transaction.debit_account
      dr_amt = transaction.debit_amount*(-1)
      cr_acc = transaction.credit_account
      cr_amt = transaction.credit_amount

      if not update_working_balance dr_acc, dr_amt
        puts "failed ---------------------- "
        return false
      end
      update_working_balance cr_acc, cr_amt
      
      return true
    end

    # update relevant accounts for online transactions
    def online_transact transaction
      dr_acc = transaction.debit_account
      dr_amt = transaction.debit_amount
      cr_acc = transaction.credit_account
      cr_amt = transaction.credit_amount

      dr_account = Account.find(dr_acc)
      cr_account = Account.find(cr_acc)

      dr_account.working_balance -= dr_amt
      dr_account.actual_balance -= dr_amt
      dr_account.last_debit = dr_amt

      cr_account.working_balance += cr_amt
      cr_account.actual_balance += cr_amt
      cr_account.last_debit = cr_amt
      
      balance_status = (dr_account.working_balance < 0 or dr_account.actual_balance < 0)
      puts "debit_amount----------------- " + dr_amt.to_s
      puts "working_balance----------------- " + dr_account.working_balance.to_s
      puts "working_balance----------------- " + (dr_account.working_balance < 0).to_s
      puts "actual_balance----------------- " + dr_account.actual_balance.to_s
      puts "actual_balance----------------- " + (dr_account.actual_balance < 0).to_s
      puts "balance_status----------------- " + balance_status.to_s
      puts "type----------------- " + ["saving", "fix_deposit", "current"].include?(dr_account.account_type).to_s

      if (["saving", "fix_deposit", "current"].include?(dr_account.account_type) and balance_status)
        return false
      end
      
      dr_account.save
      cr_account.save

      return true
    end

    # update working balance for a transaction
    def update_working_balance account_number, transaction_amount
      account = Account.find(account_number)
      account.working_balance += transaction_amount

      if (["saving", "fix_deposit", "current"].include?(account.account_type) and (account.working_balance < 0))
        return false
      else
        account.save
      end

      return true
    end

    # update actual balance of the relevant accounts when a transaction is authorised
    def authorise_transaction transaction
      if not update_dr_acc_bal(transaction.debit_account, transaction.debit_amount)
        return false
      end
      update_cr_acc_bal transaction.credit_account, transaction.credit_amount

      return true
    end

    # update actual account balance and last debit transaction amount
    def update_dr_acc_bal account_number, transaction_amount
      account = Account.find(account_number)
      account.actual_balance -= transaction_amount
      account.last_debit = transaction_amount

      if (["saving", "fix_deposit", "current"].include?(account.account_type) and (account.actual_balance < 0))
        return false
      else
        account.save
      end
      return true
      # account.save
      # puts "account working balance --------------- " + account.actual_balance.to_s
    end

    # update actual account balance and last credit transaction amount
    def update_cr_acc_bal account_number, transaction_amount
      account = Account.find(account_number)
      account.actual_balance += transaction_amount
      account.last_credit = transaction_amount
      account.save
      # puts "account working balance --------------- " + account.actual_balance.to_s
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_transaction
      @transaction = Transaction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def transaction_params
      params.require(:transaction).permit(:debit_account, :credit_account, :debit_amount, :credit_amount, 
        :effective_date, :transaction_type, :created_by, :working_balance, :actual_balance, :external_ref)
    end

    def auth_params
      params.permit(:status, :authorised_by, :authorised_date)
    end

    # validate input
    def validate_input transaction, txn_params
    # query up if account has the existing account number
      debit = Account.find_by_id(transaction.debit_account)
      credit = Account.find_by_id(transaction.credit_account)

      puts "debit account " + debit.to_s + "  -  credit account " + credit.to_s

      message = nil

      if transaction.debit_account.nil?
        message = 'Please insert a debit account number!'
      elsif debit.nil?
        message = 'Debit account number does not exist!'
      # elsif txn_params[:credit_account].empty?
      elsif transaction.credit_account.nil?
        message = 'Please insert a credit account number!' 
      elsif credit.nil?
        message = 'Credit account number does not exists!'
      elsif transaction.debit_amount.nil?
        message = 'Please input transaction amount'
      elsif transaction.credit_account.nil?
        message = 'Please input transaction amount'
      end

      return message
    end

    # return a success save statu
    def save_success
        return 'Transaction was successfully created.'
    end

    # return a failed save status
    def save_failed
        return 'Error: unable to save the record!' 
    end

    def failed_create
      return 'Insufficient account balance'
    end

    # save a new transaction and flash a status
    def save status, render_sucess, render_failed, online_status
      if not status.nil?
        flash[:notice] = status
        render :action=>render_failed
      elsif not online_status and not transact(@transaction)
        flash[:notice] = failed_create
        render :action=>render_failed
      elsif online_status and not online_transact(@transaction)
        flash[:notice] = failed_create
        render :action=>render_failed
      elsif @transaction.save
        flash[:notice] = save_success
        render :action=>render_sucess
      else
        flash[:notice] = save_failed
        render :action=>render_failed
      end
    end
end
