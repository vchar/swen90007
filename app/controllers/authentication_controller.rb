class AuthenticationController < ApplicationController
  # Turn off user authentication for all actions in this controller
  # skip_before_filer :authenticate_user!
  before_action :authenticate_admin!

  # list all users
  def index
  	@users = User.all
  end

  # show a user
  def show
  	@user = User.find(params[:id])
  end

  # create a new user
  def new
    @user = User.new()
  end

  def create
    @user = User.new(user_params)

    if @user.save
      redirect_to(:action => 'index')
    else
      render('new')
    end
  end


  # edit a user
  def edit
    @user = User.find(params[:id])
  end

  def update
    # find an existing object using form parameters
    @user = User.find(params[:id])
    # update the object
    if @user.update_attributes(user_params)
      redirect_to(:actions => 'show', :id => @user.id)
    else
      # if update fails, rediplay the form so user can fix problems
      render('edit')
    end
  end


  # delete a user
  def delete
  end

  def destroy
  end

  private
    def user_params
      # params.require(:user).permit(:username, :email)
      # params.permit(:username, :email)
    end
end
