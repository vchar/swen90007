class Admins::RegistrationsController < Devise::RegistrationsController
# before_action :configure_sign_up_params, only: [:create]
# before_action :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  # def new
  #   super
  # end

  # POST /resource
  # def create
  #   super
  # end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  # def update
  #   super
  # end


#                    Prefix Verb   URI Pattern                     Controller#Action
#         new_admin_session GET    /admins/sign_in(.:format)       devise/sessions#new
#             admin_session POST   /admins/sign_in(.:format)       devise/sessions#create
#     destroy_admin_session DELETE /admins/sign_out(.:format)      devise/sessions#destroy
#            admin_password POST   /admins/password(.:format)      devise/passwords#create
#        new_admin_password GET    /admins/password/new(.:format)  devise/passwords#new
#       edit_admin_password GET    /admins/password/edit(.:format) devise/passwords#edit
#                           PATCH  /admins/password(.:format)      devise/passwords#update
#                           PUT    /admins/password(.:format)      devise/passwords#update
# cancel_admin_registration GET    /admins/cancel(.:format)        devise/registrations#cancel
#        admin_registration POST   /admins(.:format)               devise/registrations#create

#    new_admin_registration GET    /admins/sign_up(.:format)       devise/registrations#new
#   edit_admin_registration GET    /admins/edit(.:format)          devise/registrations#edit

#                           PATCH  /admins(.:format)               devise/registrations#update
#                           PUT    /admins(.:format)               devise/registrations#update
#                           DELETE /admins(.:format)               devise/registrations#destroy


  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_up_params
  #   devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute])
  # end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  # end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
end
