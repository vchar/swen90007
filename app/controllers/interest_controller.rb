class InterestController < ApplicationController

	before_action :set_account

	# show interest info
	# not used...
	def show
		@interest = Interest.find(params[:id])
	end

	def index
		@interest = Interest.all
	end

	private
	# Use callbacks to share common setup or constraints between actions.
    def set_account
		@user = current_user
		@accounts = current_user.accounts
    end

end
