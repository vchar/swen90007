class ApplicationController < ActionController::Base
  before_action :authenticate_user!
  before_action :permitted_params, if: :devise_controller?
  protect_from_forgery with: :exception

  protected
  	def permitted_params
  		added_attrs = [:username, :email, :password]
  		updated_attrs = [:first_name, :last_name, :role, :address, :contact_number, :status, 
  			:position, :department, :occupation, :company, :created_by, :updated_by]
  		devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
  		devise_parameter_sanitizer.permit :account_update, keys: updated_attrs
  	end
end