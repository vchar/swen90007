SWEN90007 Software Architecture and Design
------------------------------------------


# OMG Banking Project - Ruby on Rails 5

This is the prototype for the project of SWEN90007
by Veasna Char - 668246 and Jone Chee - 634350


## Requirement
* Rails 5.0.0.1


## Getting started

To get started with the app, clone the repo and then install the needed gems:

```
$ bundle install --without production
```

Next, migrate the database:

```
$ rails db:migrate
```

Then, seed data

```
$ rails db:seed
```

Y'll be ready to run the app in a local server:

```
$ rails server
```


## Interest Task
```
# with auto date
$ rake give_interest:auto

# specifying dates
$ rake give_interest:by_dates[3/4/2016,4/5/2018]

# specifying date and number of month
$ rake give_interest:by_month[12/3/2014,4]

```